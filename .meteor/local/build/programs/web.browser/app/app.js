var require = meteorInstall({"imports":{"components":{"todosList":{"todosList.html":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// imports/components/todosList/todosList.html                                                    //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
                                                                                                  // 1
      if (Meteor.isServer) return;                                                                // 2
                                                                                                  // 3
      var templateUrl = "/imports/components/todosList/todosList.html";                           // 4
      var template = "<header> <h1>Todo List ( {{$ctrl.incompleteCount}} )</h1> <label class=\"hide-completed\"> <input type=\"checkbox\" ng-model=\"$ctrl.hideCompleted\"> Hide Completed Tasks </label> <login-buttons></login-buttons> <form class=\"new-task\" ng-submit=\"$ctrl.addTask($ctrl.newTask)\" ng-show=\"$ctrl.currentUser\"> <input ng-model=\"$ctrl.newTask\" type=\"text\" name=\"text\" placeholder=\"Type to add new tasks\"> </form> </header> <ul> <li ng-repeat=\"task in $ctrl.tasks\" ng-class=\"{'checked': task.checked, 'private': task.private}\"> <button class=\"delete\" ng-click=\"$ctrl.removeTask(task)\">&times;</button> <input type=\"checkbox\" ng-checked=\"task.checked\" ng-click=\"$ctrl.setChecked(task)\" class=\"toggle-checked\"> <span class=\"text\"> <strong>{{task.username}}</strong> - {{task.text}} </span> <button class=\"toggle-private\" ng-click=\"$ctrl.setPrivate(task)\" ng-show=\"task.owner === $ctrl.currentUser._id\"> {{task.private == true ? \"Private\" : \"Public\"}} </button> </li></ul> ";
                                                                                                  // 6
      angular.module('angular-templates')                                                         // 7
        .run(['$templateCache', function($templateCache) {                                        // 8
          $templateCache.put(templateUrl, template);                                              // 9
        }]);                                                                                      // 10
                                                                                                  // 11
      module.exports = {};                                                                        // 12
      module.exports.__esModule = true;                                                           // 13
      module.exports.default = templateUrl;                                                       // 14
                                                                                                  // 15
////////////////////////////////////////////////////////////////////////////////////////////////////

},"todosList.js":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// imports/components/todosList/todosList.js                                                      //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");                           //
                                                                                                  //
var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);                                  //
                                                                                                  //
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
                                                                                                  //
var module1 = module;                                                                             // 1
var angular = void 0;                                                                             // 1
module1.watch(require("angular"), {                                                               // 1
  "default": function (v) {                                                                       // 1
    angular = v;                                                                                  // 1
  }                                                                                               // 1
}, 0);                                                                                            // 1
var angularMeteor = void 0;                                                                       // 1
module1.watch(require("angular-meteor"), {                                                        // 1
  "default": function (v) {                                                                       // 1
    angularMeteor = v;                                                                            // 1
  }                                                                                               // 1
}, 1);                                                                                            // 1
var Tasks = void 0;                                                                               // 1
module1.watch(require("../../api/tasks.js"), {                                                    // 1
  Tasks: function (v) {                                                                           // 1
    Tasks = v;                                                                                    // 1
  }                                                                                               // 1
}, 2);                                                                                            // 1
var Meteor = void 0;                                                                              // 1
module1.watch(require("meteor/meteor"), {                                                         // 1
  Meteor: function (v) {                                                                          // 1
    Meteor = v;                                                                                   // 1
  }                                                                                               // 1
}, 3);                                                                                            // 1
var template = void 0;                                                                            // 1
module1.watch(require("./todosList.html"), {                                                      // 1
  "default": function (v) {                                                                       // 1
    template = v;                                                                                 // 1
  }                                                                                               // 1
}, 4);                                                                                            // 1
                                                                                                  //
var TodosListCtrl = function () {                                                                 //
  function TodosListCtrl($scope) {                                                                // 9
    (0, _classCallCheck3.default)(this, TodosListCtrl);                                           // 9
    $scope.viewModel(this);                                                                       // 10
    this.subscribe('tasks');                                                                      // 12
    this.hideCompleted = false;                                                                   // 14
    this.helpers({                                                                                // 16
      tasks: function () {                                                                        // 17
        var selector = {};                                                                        // 18
                                                                                                  //
        if (this.getReactively('hideCompleted')) {                                                // 20
          selector.checked = {                                                                    // 21
            $ne: true                                                                             // 22
          };                                                                                      // 21
        } // Show newest tasks at the top                                                         // 24
                                                                                                  //
                                                                                                  //
        return Tasks.find(selector, {                                                             // 27
          sort: {                                                                                 // 28
            createdAt: -1                                                                         // 29
          }                                                                                       // 28
        });                                                                                       // 27
      },                                                                                          // 32
      incompleteCount: function () {                                                              // 33
        return Tasks.find({                                                                       // 34
          checked: {                                                                              // 35
            $ne: true                                                                             // 36
          }                                                                                       // 35
        }).count();                                                                               // 34
      },                                                                                          // 39
      currentUser: function () {                                                                  // 40
        return Meteor.user();                                                                     // 41
      }                                                                                           // 42
    });                                                                                           // 16
  }                                                                                               // 44
                                                                                                  //
  TodosListCtrl.prototype.addTask = function () {                                                 //
    function addTask(newTask) {                                                                   //
      // Insert a task into the collection                                                        // 47
      Meteor.call('tasks.insert', newTask); // Clear form                                         // 48
                                                                                                  //
      this.newTask = '';                                                                          // 51
    }                                                                                             // 52
                                                                                                  //
    return addTask;                                                                               //
  }();                                                                                            //
                                                                                                  //
  TodosListCtrl.prototype.setChecked = function () {                                              //
    function setChecked(task) {                                                                   //
      // Set the checked property to the opposite of its current value                            // 55
      Meteor.call('tasks.setChecked', task._id, !task.checked);                                   // 56
    }                                                                                             // 57
                                                                                                  //
    return setChecked;                                                                            //
  }();                                                                                            //
                                                                                                  //
  TodosListCtrl.prototype.removeTask = function () {                                              //
    function removeTask(task) {                                                                   //
      Meteor.call('tasks.remove', task._id);                                                      // 60
    }                                                                                             // 61
                                                                                                  //
    return removeTask;                                                                            //
  }();                                                                                            //
                                                                                                  //
  TodosListCtrl.prototype.setPrivate = function () {                                              //
    function setPrivate(task) {                                                                   //
      Meteor.call('tasks.setPrivate', task._id, !task.private);                                   // 64
    }                                                                                             // 65
                                                                                                  //
    return setPrivate;                                                                            //
  }();                                                                                            //
                                                                                                  //
  return TodosListCtrl;                                                                           //
}();                                                                                              //
                                                                                                  //
module1.exportDefault(angular.module('todosList', [angularMeteor]).component('todosList', {       // 1
  templateUrl: 'imports/components/todosList/todosList.html',                                     // 72
  controller: TodosListCtrl                                                                       // 73
}));                                                                                              // 71
////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"api":{"tasks.js":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// imports/api/tasks.js                                                                           //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
module.export({                                                                                   // 1
  Tasks: function () {                                                                            // 1
    return Tasks;                                                                                 // 1
  }                                                                                               // 1
});                                                                                               // 1
var Meteor = void 0;                                                                              // 1
module.watch(require("meteor/meteor"), {                                                          // 1
  Meteor: function (v) {                                                                          // 1
    Meteor = v;                                                                                   // 1
  }                                                                                               // 1
}, 0);                                                                                            // 1
var Mongo = void 0;                                                                               // 1
module.watch(require("meteor/mongo"), {                                                           // 1
  Mongo: function (v) {                                                                           // 1
    Mongo = v;                                                                                    // 1
  }                                                                                               // 1
}, 1);                                                                                            // 1
var check = void 0;                                                                               // 1
module.watch(require("meteor/check"), {                                                           // 1
  check: function (v) {                                                                           // 1
    check = v;                                                                                    // 1
  }                                                                                               // 1
}, 2);                                                                                            // 1
var Tasks = new Mongo.Collection('tasks');                                                        // 5
                                                                                                  //
if (Meteor.isServer) {                                                                            // 7
  // This code only runs on the server                                                            // 8
  // Only publish tasks that are public or belong to the current user                             // 9
  Meteor.publish('tasks', function () {                                                           // 10
    function tasksPublication() {                                                                 // 10
      return Tasks.find({                                                                         // 11
        $or: [{                                                                                   // 12
          "private": {                                                                            // 13
            $ne: true                                                                             // 14
          }                                                                                       // 13
        }, {                                                                                      // 12
          owner: this.userId                                                                      // 17
        }]                                                                                        // 16
      });                                                                                         // 11
    }                                                                                             // 20
                                                                                                  //
    return tasksPublication;                                                                      // 10
  }());                                                                                           // 10
}                                                                                                 // 21
                                                                                                  //
Meteor.methods({                                                                                  // 23
  'tasks.insert': function (text) {                                                               // 24
    check(text, String); // Make sure the user is logged in before inserting a task               // 25
                                                                                                  //
    if (!Meteor.userId()) {                                                                       // 28
      throw new Meteor.Error('not-authorized');                                                   // 29
    }                                                                                             // 30
                                                                                                  //
    Tasks.insert({                                                                                // 32
      text: text,                                                                                 // 33
      createdAt: new Date(),                                                                      // 34
      owner: Meteor.userId(),                                                                     // 35
      username: Meteor.user().username                                                            // 36
    });                                                                                           // 32
  },                                                                                              // 38
  'tasks.remove': function (taskId) {                                                             // 39
    check(taskId, String);                                                                        // 40
    var task = Tasks.findOne(taskId);                                                             // 42
                                                                                                  //
    if (task.private && task.owner !== Meteor.userId()) {                                         // 43
      // If the task is private, make sure only the owner can delete it                           // 44
      throw new Meteor.Error('not-authorized');                                                   // 45
    }                                                                                             // 46
                                                                                                  //
    Tasks.remove(taskId);                                                                         // 48
  },                                                                                              // 49
  'tasks.setChecked': function (taskId, setChecked) {                                             // 50
    check(taskId, String);                                                                        // 51
    check(setChecked, Boolean);                                                                   // 52
    var task = Tasks.findOne(taskId);                                                             // 54
                                                                                                  //
    if (task.private && task.owner !== Meteor.userId()) {                                         // 55
      // If the task is private, make sure only the owner can check it off                        // 56
      throw new Meteor.Error('not-authorized');                                                   // 57
    }                                                                                             // 58
                                                                                                  //
    Tasks.update(taskId, {                                                                        // 60
      $set: {                                                                                     // 61
        checked: setChecked                                                                       // 62
      }                                                                                           // 61
    });                                                                                           // 60
  },                                                                                              // 65
  'tasks.setPrivate': function (taskId, setToPrivate) {                                           // 67
    check(taskId, String);                                                                        // 68
    check(setToPrivate, Boolean);                                                                 // 69
    var task = Tasks.findOne(taskId); // Make sure only the task owner can make a task private    // 71
                                                                                                  //
    if (task.owner !== Meteor.userId()) {                                                         // 74
      throw new Meteor.Error('not-authorized');                                                   // 75
    }                                                                                             // 76
                                                                                                  //
    Tasks.update(taskId, {                                                                        // 78
      $set: {                                                                                     // 79
        "private": setToPrivate                                                                   // 80
      }                                                                                           // 79
    });                                                                                           // 78
  }                                                                                               // 83
});                                                                                               // 23
////////////////////////////////////////////////////////////////////////////////////////////////////

}},"startup":{"accounts-config.js":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// imports/startup/accounts-config.js                                                             //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
var Accounts = void 0;                                                                            // 1
module.watch(require("meteor/accounts-base"), {                                                   // 1
  Accounts: function (v) {                                                                        // 1
    Accounts = v;                                                                                 // 1
  }                                                                                               // 1
}, 0);                                                                                            // 1
Accounts.ui.config({                                                                              // 3
  passwordSignupFields: 'USERNAME_ONLY'                                                           // 4
});                                                                                               // 3
////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"client":{"main.html.js":function(){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// client/main.html.js                                                                            //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
                                                                                                  // 1
            Meteor.startup(function() {                                                           // 2
              var attrs = {};                                                                     // 3
              for (var prop in attrs) {                                                           // 4
                document.body.setAttribute(prop, attrs[prop]);                                    // 5
              }                                                                                   // 6
            });                                                                                   // 7
                                                                                                  // 8
////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":function(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// client/main.js                                                                                 //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
var module1 = module;                                                                             // 1
var angular = void 0;                                                                             // 1
module1.watch(require("angular"), {                                                               // 1
  "default": function (v) {                                                                       // 1
    angular = v;                                                                                  // 1
  }                                                                                               // 1
}, 0);                                                                                            // 1
var angularMeteor = void 0;                                                                       // 1
module1.watch(require("angular-meteor"), {                                                        // 1
  "default": function (v) {                                                                       // 1
    angularMeteor = v;                                                                            // 1
  }                                                                                               // 1
}, 1);                                                                                            // 1
var todosList = void 0;                                                                           // 1
module1.watch(require("../imports/components/todosList/todosList"), {                             // 1
  "default": function (v) {                                                                       // 1
    todosList = v;                                                                                // 1
  }                                                                                               // 1
}, 2);                                                                                            // 1
module1.watch(require("../imports/startup/accounts-config.js"));                                  // 1
angular.module('simple-todos', [angularMeteor, todosList.name, 'accounts.ui']);                   // 6
////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".html",
    ".css"
  ]
});
require("./client/main.html.js");
require("./client/main.js");