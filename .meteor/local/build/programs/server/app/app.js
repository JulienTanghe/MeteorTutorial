var require = meteorInstall({"imports":{"api":{"tasks.js":function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                             //
// imports/api/tasks.js                                                                        //
//                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                               //
module.export({                                                                                // 1
  Tasks: function () {                                                                         // 1
    return Tasks;                                                                              // 1
  }                                                                                            // 1
});                                                                                            // 1
var Meteor = void 0;                                                                           // 1
module.watch(require("meteor/meteor"), {                                                       // 1
  Meteor: function (v) {                                                                       // 1
    Meteor = v;                                                                                // 1
  }                                                                                            // 1
}, 0);                                                                                         // 1
var Mongo = void 0;                                                                            // 1
module.watch(require("meteor/mongo"), {                                                        // 1
  Mongo: function (v) {                                                                        // 1
    Mongo = v;                                                                                 // 1
  }                                                                                            // 1
}, 1);                                                                                         // 1
var check = void 0;                                                                            // 1
module.watch(require("meteor/check"), {                                                        // 1
  check: function (v) {                                                                        // 1
    check = v;                                                                                 // 1
  }                                                                                            // 1
}, 2);                                                                                         // 1
var Tasks = new Mongo.Collection('tasks');                                                     // 5
                                                                                               //
if (Meteor.isServer) {                                                                         // 7
  // This code only runs on the server                                                         // 8
  // Only publish tasks that are public or belong to the current user                          // 9
  Meteor.publish('tasks', function () {                                                        // 10
    function tasksPublication() {                                                              // 10
      return Tasks.find({                                                                      // 11
        $or: [{                                                                                // 12
          "private": {                                                                         // 13
            $ne: true                                                                          // 14
          }                                                                                    // 13
        }, {                                                                                   // 12
          owner: this.userId                                                                   // 17
        }]                                                                                     // 16
      });                                                                                      // 11
    }                                                                                          // 20
                                                                                               //
    return tasksPublication;                                                                   // 10
  }());                                                                                        // 10
}                                                                                              // 21
                                                                                               //
Meteor.methods({                                                                               // 23
  'tasks.insert': function (text) {                                                            // 24
    check(text, String); // Make sure the user is logged in before inserting a task            // 25
                                                                                               //
    if (!Meteor.userId()) {                                                                    // 28
      throw new Meteor.Error('not-authorized');                                                // 29
    }                                                                                          // 30
                                                                                               //
    Tasks.insert({                                                                             // 32
      text: text,                                                                              // 33
      createdAt: new Date(),                                                                   // 34
      owner: Meteor.userId(),                                                                  // 35
      username: Meteor.user().username                                                         // 36
    });                                                                                        // 32
  },                                                                                           // 38
  'tasks.remove': function (taskId) {                                                          // 39
    check(taskId, String);                                                                     // 40
    var task = Tasks.findOne(taskId);                                                          // 42
                                                                                               //
    if (task.private && task.owner !== Meteor.userId()) {                                      // 43
      // If the task is private, make sure only the owner can delete it                        // 44
      throw new Meteor.Error('not-authorized');                                                // 45
    }                                                                                          // 46
                                                                                               //
    Tasks.remove(taskId);                                                                      // 48
  },                                                                                           // 49
  'tasks.setChecked': function (taskId, setChecked) {                                          // 50
    check(taskId, String);                                                                     // 51
    check(setChecked, Boolean);                                                                // 52
    var task = Tasks.findOne(taskId);                                                          // 54
                                                                                               //
    if (task.private && task.owner !== Meteor.userId()) {                                      // 55
      // If the task is private, make sure only the owner can check it off                     // 56
      throw new Meteor.Error('not-authorized');                                                // 57
    }                                                                                          // 58
                                                                                               //
    Tasks.update(taskId, {                                                                     // 60
      $set: {                                                                                  // 61
        checked: setChecked                                                                    // 62
      }                                                                                        // 61
    });                                                                                        // 60
  },                                                                                           // 65
  'tasks.setPrivate': function (taskId, setToPrivate) {                                        // 67
    check(taskId, String);                                                                     // 68
    check(setToPrivate, Boolean);                                                              // 69
    var task = Tasks.findOne(taskId); // Make sure only the task owner can make a task private
                                                                                               //
    if (task.owner !== Meteor.userId()) {                                                      // 74
      throw new Meteor.Error('not-authorized');                                                // 75
    }                                                                                          // 76
                                                                                               //
    Tasks.update(taskId, {                                                                     // 78
      $set: {                                                                                  // 79
        "private": setToPrivate                                                                // 80
      }                                                                                        // 79
    });                                                                                        // 78
  }                                                                                            // 83
});                                                                                            // 23
/////////////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"main.js":function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                             //
// server/main.js                                                                              //
//                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                               //
var Meteor = void 0;                                                                           // 1
module.watch(require("meteor/meteor"), {                                                       // 1
  Meteor: function (v) {                                                                       // 1
    Meteor = v;                                                                                // 1
  }                                                                                            // 1
}, 0);                                                                                         // 1
module.watch(require("../imports/api/tasks.js"));                                              // 1
Meteor.startup(function () {// code to run on server at startup                                // 4
});                                                                                            // 6
/////////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".html"
  ]
});
require("./server/main.js");
//# sourceMappingURL=app.js.map
